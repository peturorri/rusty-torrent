
use std::fmt::Write;

/*
 * XXX: This method is terribly inefficient.
 */
pub fn urlencode_bytes(bytes: &[u8]) -> String {
    let mut s = String::new();

    for b in bytes {
        s.push_str(&encode_byte(*b));
    }

    s
}

/*
 * https://url.spec.whatwg.org/#urlencoded-serializing
 */
pub fn encode_byte(b: u8) -> String {
    if b == 0x20 {
        // A space is encoded as a plus sign.
        String::from("+")
    }
    else if b == 0x2a
        || b == 0x2d
        || b == 0x2e
        || (b >= 0x30 && b <= 0x39)
        || (b >= 0x41 && b <= 0x5a)
        || b == 0x5f
        || (b >= 0x61 && b <= 0x7a)
    {
        /*
        These values should be written straight out.
        0x2A
        0x2D
        0x2E
        0x30 to 0x39
        0x41 to 0x5A
        0x5F
        0x61 to 0x7A 
        */
        let ret: String = match String::from_utf8(vec!(b)) {
            Ok(s) => s,
            Err(_) => panic!("This really shouldn't have happened.")
        };
        ret
    }
    else {
        // Percent encode anything else.
        let mut s = String::new();
        write!(&mut s, "%{:02X}", b).unwrap();
        s
    }
}
