
extern crate rusty_torrent;

extern crate hyper;
extern crate url;

use std::env;

use rusty_torrent::torrent::open_torrent_file;
use rusty_torrent::urlencode::urlencode_bytes;

//use hyper::Client;
use hyper::client::Response;
use hyper::client::Client;

use std::io::Read;


fn main() {
    let args: Vec<_> = env::args().collect();

    if args.len() < 2 {
        panic!("Missing filename argument.");
    }

    let info = match open_torrent_file(&args[1]) {
        Ok(i) => i,
        Err(e) => {
            println!("Error while opening torrent file {}: {}", &args[1], e);
            return;
        }
    };
    
    println!("{:?}", info.announce);
    println!("{:?}", info.info_hash);
    println!("{:?}", urlencode_bytes(&info.info_hash));

    let client = Client::new();

    let mut request_url = url::Url::parse(&info.announce).unwrap();
    // Set the info_hash manually since the url crate currently doesn't support setting arbitrary
    // bytes. See: https://github.com/servo/rust-url/issues/219
    request_url.set_query(Some(&format!("info_hash={}", &urlencode_bytes(&info.info_hash))));

    request_url.query_pairs_mut()
        .append_pair("peer_id", "-RU0000-aaaaaaaaaaaa")
        .append_pair("port", "6889")
        .append_pair("uploaded", "0")
        .append_pair("downloaded", "0")
        .append_pair("left", "100")
        .append_pair("compact", "0")
        .append_pair("no_peer_id", "0")
        .append_pair("event", "started")
        .append_pair("numwant", "3")
    ;


    let mut res = client.get(request_url).send().unwrap();

    println!("{:?}", res);

    let mut res_body = Vec::new();
    res.read_to_end(&mut res_body).unwrap();
    println!("{:?}", res_body);
    println!("{:?}", String::from_utf8_lossy(&res_body));
}
