
use std::str;
use std::collections::HashMap;
use std::result::Result;

// XXX: the spec does not actually have a size limit.
// TODO: check out the 'num' crate
pub type BencIntType = i64;

// Note: benc strings are not utf8 strings. They are arbitrary byte arrays. In particular they
// contain hashes.
#[derive(Debug)]
pub enum BencValue {
    String(Vec<u8>),
    Int(BencIntType),
    List(Vec<BencValue>),
    Dict(HashMap<Vec<u8>, BencValue>),
}


pub fn parse_benc(b: &[u8]) -> Result<BencValue, &str> {
    match parse_benc_internal(b) {
        Ok((v, _)) => Ok(v),
        Err(e) => Err(e)
    }
}

pub fn parse_benc_internal(b: &[u8]) -> Result<(BencValue, &[u8]), &str> {
    let next_char: u8 = match b.first() {
        Some(n) => *n,
        None => return Err("Tried to decode an empty buffer.")
    };
    
    match next_char {
        b'0' | b'1' | b'2' | b'3' | b'4' | b'5' | b'6' | b'7' | b'8' | b'9' => Ok(try!(parse_benc_string(b))),
        b'i' => Ok(try!(parse_benc_int(b))),
        b'l' => Ok(try!(parse_benc_list(b))),
        b'd' => Ok(try!(parse_benc_dict(b))),
        _   => Err("Invalid start of item.")
    }
}

pub fn parse_benc_string(b: &[u8]) -> Result<(BencValue, &[u8]), &str> {

    let colon_pos = match b.iter().position(|&c| c == b':') {
        None => return Err("Invalid string item while decoding."),
        Some(i) => i
    };

    let len_str = match str::from_utf8(&b[0..colon_pos]) {
        Err(_) => return Err("Couldn't decode string length (1)."),
        Ok(l) => l
    };

    let len = match len_str.parse::<usize>() {
        Err(_) => return Err("Couldn't decode string length (2)."),
        Ok(l) => l
    };

    let value_start_pos = match colon_pos.checked_add(1) {
        None => return Err("Integer overflow while parsing string."),
        Some(p) => p
    };
    let value_end_pos = match value_start_pos.checked_add(len) {
        None => return Err("Integer overflow while parsing string."),
        Some(p) => p
    };

    if b.len() < value_end_pos {
        return Err("Parsed string length bigger than remaining string.");
    }

    let mut ret = vec!();
    ret.extend_from_slice(&b[value_start_pos..value_end_pos]);

    Ok((BencValue::String(ret), &b[value_end_pos..]))
}

pub fn parse_benc_int(b: &[u8]) -> Result<(BencValue, &[u8]), &str> {
    match b.first() {
        Some(first) => {
            if *first != b'i' {
                return Err("Something not beginning with 'i' got into the int decoder.");
            }
        },
        None => return Err("An empty string got into the int decoder.")
    };

    let e_pos = match b.iter().position(|&c| c == b'e') {
        None => return Err("Invalid int item while decoding."),
        Some(p) => p
    };

    let val_str = match str::from_utf8(&b[1..e_pos]) {
        Err(_) => return Err("Couldn't decode int."),
        Ok(v) => v
    };

    let val = match val_str.parse::<BencIntType>() {
        Err(_) => return Err("Couldn't decode int."),
        Ok(p) => p
    };

    // It would be pretty hard to cause this overflow to actually happen, but you never know.
    let rest_pos = match e_pos.checked_add(1) {
        None => return Err("Integer overflow while parsing int."),
        Some(p) => p
    };

    Ok((BencValue::Int(val), &b[rest_pos..]))
}

pub fn parse_benc_list(b: &[u8]) -> Result<(BencValue, &[u8]), &str> {
    match b.first() {
        Some(first) => {
            if *first != b'l' {
                return Err("Something not beginning with 'l' got into the list decoder.");
            }
        },
        None => return Err("An empty string got into the list decoder.")
    };

    let mut rest = &b[1..];

    let mut list: Vec<BencValue> = Vec::new();

    loop {
        let next_char = match rest.first() {
            None => return Err("Ran out of stuff while parsing list."),
            Some(p) => *p
        };

        if next_char == b'e' {
            rest = &rest[1..];
            break;
        }

        let (next_val, new_rest) = try!(parse_benc_internal(rest));
        rest = new_rest;
        list.push(next_val);
    }

    Ok((BencValue::List(list), rest))
}

pub fn parse_benc_dict(b: &[u8]) -> Result<(BencValue, &[u8]), &str> {
    match b.first() {
        Some(first) => {
            if *first != b'd' {
                return Err("Something not beginning with 'd' got into the dict decoder.");
            }
        },
        None => return Err("An empty string got into the dict decoder.")
    };

    let mut rest = &b[1..];
    let mut dict: HashMap<Vec<u8>, BencValue> = HashMap::new();

    loop {
        let next_char = match rest.first() {
            None => return Err("Ran out of stuff while parsing dict."),
            Some(p) => *p
        };

        if next_char == b'e' {
            rest = &rest[1..];
            break;
        }

        let (key_wrapped, new_rest_key) = try!(parse_benc_string(rest));
        let key = match key_wrapped {
            BencValue::String(s) => s,
            _ => return Err("String parsing function returned something that wasn't a string!")
        };

        let (value, new_rest) = try!(parse_benc_internal(new_rest_key));
        rest = new_rest;
        dict.insert(key, value);
    }

    Ok((BencValue::Dict(dict), rest))
}

/*
 * Get the raw value of a key in a bencoded dict.
 * This is useful for getting the info_hash value used in BitTorrent.
 * Return None if the key isn't present.
 */
pub fn get_raw_value<'a, 'b>(b: &'a [u8], target_key: &'b [u8]) -> Result<Option<&'a [u8]>, &'a str> {
    match b.first() {
        Some(first) => {
            if *first != b'd' {
                return Err("Not a dict.");
            }
        },
        None => return Ok(None) // Empty dict implies that key isn't present.
    };

    let mut rest = &b[1..];

    loop {
        let next_char = match rest.first() {
            None => return Err("Ran out of stuff while parsing dict."),
            Some(p) => *p
        };

        // If we've reached the end of the dict without returning the desired value, it's not
        // present.
        if next_char == b'e' {
            return Ok(None);
        }

        let (key_wrapped, new_rest_key) = try!(parse_benc_string(rest));
        let target_found = match key_wrapped {
            BencValue::String(s) => s == target_key,
            _ => return Err("String parsing function returned something that wasn't a string!")
        };

        let (_, new_rest) = try!(parse_benc_internal(new_rest_key));

        if target_found { 
            return Ok(Some(&new_rest_key[0..(new_rest_key.len()-new_rest.len())]));
        }
        else {
            rest = new_rest;
        }
    }
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn string_basic() {
        match parse_benc_string(b"4:asdf").unwrap() {
            (BencValue::String(s), r) => {
                assert_eq!(s, b"asdf");
                assert_eq!(r, b"");
            },
            _ => panic!()
        }
    }

    #[test]
    fn string_rest() {
        match parse_benc_string(b"5:valuerest").unwrap() {
            (BencValue::String(s), r) => {
                assert_eq!(s, b"value");
                assert_eq!(r, b"rest");
            },
            _ => panic!()
        }
    }

    #[test]
    fn string_too_short() {
        match parse_benc_string(b"6:short") {
            Ok(_) => panic!(),
            Err(_) => {}
        }
    }

}


















