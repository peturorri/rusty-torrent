
extern crate time;
extern crate sha1;

use std::io::prelude::*;
use std::fs::File;
use std::collections::HashMap;
use std::str;

use bencode::*;


#[derive(Debug)]
pub struct TorrentInfo {
    pub info: TorrentFileInfo,
    pub info_hash: Vec<u8>,
    pub announce: String,
    pub announce_list: Option<Vec<BencValue>>,
    pub creation_date: Option<time::Timespec>,
    pub comment: Option<String>,
    pub created_by: Option<String>,
    pub encoding: Option<String>
}

#[derive(Debug)]
pub enum TorrentFileInfo {
    Single(SingleFile),
    Multiple(MultipleFiles)
}

#[derive(Debug)]
pub struct SingleFile {
    piece_length: i64,
    pieces: Vec<u8>,
    private: Option<bool>,
    name: String,
    length: i64,
    md5sum: Option<Vec<u8>>,
}

#[derive(Debug)]
pub struct MultipleFiles {
}

pub fn open_torrent_file(filename: &str) -> Result<TorrentInfo, &str> {
    let mut f = match File::open(filename) {
        Ok(f) => f,
        Err(_) => return Err("Error while opening torrent file.")
    };

    let mut buf = vec![];
    match f.read_to_end(&mut buf) {
        Ok(_) => {},
        Err(_) => return Err("Error while reading torrent file.")
    };

    let parse_result = match parse_benc(&buf) {
        Ok(v) => v,
        Err(_) => return Err("Error while parsing torrent file.")
    };

    let info_hash = match get_info_hash(&buf) {
        Ok(v) => v,
        Err(_) => return Err("Error while getting info_hash.")
    };
    
    let mut metainfo = match parse_result {
        BencValue::Dict(d) => d,
        _ => return Err("Invalid torrent file.")
    };

    // Need to annotate the type so the array doesn't get a size inferred, which causes
    // problems when trying to borrow the value further on in the code.
    // Due to the same issues it looks pretty nasty to try to use the literals straight
    // in the function calls. (You need something like &b"announce"[..] )
    let info_key: &[u8] = b"info";
    let announce_key: &[u8] = b"announce";
    let announce_list_key: &[u8] = b"announce-list";
    let creation_date_key: &[u8] = b"creation date";
    let comment_key: &[u8] = b"comment";
    let created_by_key: &[u8] = b"created by";
    let encoding_key: &[u8] = b"encoding";

    let info = match metainfo.remove(info_key) {
        Some(BencValue::Dict(d)) => match parse_info_dict(d) {
            Ok(parsed) => parsed,
            Err(e) => return Err(e)
        },
        _ => return Err("Error while reading info dict. Missing or malformed.")
    };

    let announce = match metainfo.remove(announce_key) {
        Some(BencValue::String(s_u8)) => {
            match String::from_utf8(s_u8) {
                Ok(s_string) => s_string,
                Err(_) => return Err("Error reading announce. Value not utf8")
            }
        }
        _ => return Err("Error while reading announce url. Missing or malformed.")
    };

    let announce_list: Option<Vec<BencValue>> = match metainfo.remove(announce_list_key) {
        Some(BencValue::List(l)) => Some(l),
        None => None,
        _ => return Err("Error while reading announce-list. Malformed.")
    };

    let creation_date = match metainfo.remove(creation_date_key) {
        Some(BencValue::Int(i)) => Some(time::Timespec::new(i, 0)),
        None => None,
        _ => return Err("Error while reading creation date. Malformed.")
    };

    let comment = match metainfo.remove(comment_key) {
        Some(BencValue::String(s_u8)) => {
            match String::from_utf8(s_u8) {
                Ok(s_string) => Some(s_string),
                Err(_) => return Err("Error reading comment. Value not utf8")
            }
        }
        None => None,
        _ => return Err("Error while reading comment. Malformed.")
    };

    let created_by = match metainfo.remove(created_by_key) {
        Some(BencValue::String(s_u8)) => {
            match String::from_utf8(s_u8) {
                Ok(s_string) => Some(s_string),
                Err(_) => return Err("Error reading created_by. Value not utf8")
            }
        }
        None => None,
        _ => return Err("Error while reading created by. Malformed.")
    };

    let encoding = match metainfo.remove(encoding_key) {
        Some(BencValue::String(s_u8)) => {
            match String::from_utf8(s_u8) {
                Ok(s_string) => Some(s_string),
                Err(_) => return Err("Error reading encoding. Value not utf8")
            }
        }
        None => None,
        _ => return Err("Error while reading encoding. Malformed.")
    };


    let torrent_info = TorrentInfo {
        info: info,
        info_hash: info_hash,
        announce: announce,
        announce_list: announce_list,
        creation_date: creation_date,
        comment: comment,
        created_by: created_by,
        encoding: encoding
    };

    Ok(torrent_info)
}

fn parse_info_dict(mut info: HashMap<Vec<u8>, BencValue>) -> Result<TorrentFileInfo, &'static str> {
    let piece_length_key: &[u8] = b"piece length";
    let pieces_key: &[u8] = b"pieces";
    let private_key: &[u8] = b"private";
    let name_key: &[u8] = b"name";
    let length_key: &[u8] = b"length";
    let md5sum_key: &[u8] = b"md5sum";
    //let files_key: &[u8] = b"files";
    //let path_key: &[u8] = b"path";

    let piece_length = match info.remove(piece_length_key) {
        Some(BencValue::Int(i)) => i,
        _ => return Err("Missing or invalid piece length.")
    };

    let pieces = match info.remove(pieces_key) {
        Some(BencValue::String(s)) => s,
        _ => return Err("Missing or invalid pieces.")
    };

    let private = match info.remove(private_key) {
        Some(BencValue::Int(i)) => Some(i == 1),
        None => None,
        _ => return Err("Invalid private flag.")
    };

    let name = match info.remove(name_key) {
        Some(BencValue::String(s_u8)) => {
            match String::from_utf8(s_u8) {
                Ok(s_string) => s_string,
                Err(_) => return Err("Error reading name. Value not utf8.")
            }
        },
        _ => return Err("Missing or invalid name.")
    };


    if info.contains_key(length_key) {
        // Assume we're in single file mode.
        let length = match info.remove(length_key) {
            Some(BencValue::Int(i)) => i,
            _ => return Err("Missing or invalid length.")
        };

        let md5sum = match info.remove(md5sum_key) {
            Some(BencValue::String(s)) => Some(s),
            None => None,
            _ => return Err("Invalid md5sum.")
        };
        
        // TODO: Some validation of length, pieces and file size.

        Ok(TorrentFileInfo::Single(SingleFile {
            piece_length: piece_length,
            pieces: pieces,
            private: private,
            name: name,
            length: length,
            md5sum: md5sum,
        }))
    }
    else {
        // Assume we're in multiple file mode.
        Ok(TorrentFileInfo::Multiple(MultipleFiles{}))
    }
}


fn get_info_hash(b: &[u8]) -> Result<Vec<u8>, &str> {
    let info_val = get_raw_value(b, b"info").unwrap().unwrap();

    let mut hash = sha1::Sha1::new();
    hash.update(info_val);

    Ok(hash.digest().bytes().to_vec())
}
















